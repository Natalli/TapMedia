$(document).ready(function () {
    var expand = $('.menuWrapper .childList'),
        expandOpen = $('.menuWrapper .childList.open'),
        subMenu = $('.subMenu');

    var top = $('.childList').offset().top;

    function showMenu() {
        var main = $('#main');

        $('.menuBtn').on('click', function () {
            if (main.hasClass('hideMenu')){

                main.removeClass('hideMenu');
                main.addClass('showMenu');
                expand.find('.subMenu').css('top', 'auto');
                openSubMenu();
                if ($(window).outerWidth() < 768) {
                    body.css('overflow', 'hidden');
                }
            }  else {
                HoverList();
                main.removeClass('showMenu');
                main.addClass('hideMenu');
                $(document).find('.menuWrapper .subMenu').css('top', 'auto');
                if ($(window).outerWidth() < 768) {
                    body.css('overflow', 'auto');
                }
                $(document).find('.menuWrapper .open').removeClass('open');
                expandOpen.removeClass('open');
            }

        })
    }

    showMenu();

    function HoverList() {
        expand.hover(
            function () {
                $(this).addClass('showSub');
                $(this).find('.subMenu').css('top', top);
            },
            function () {
                $(this).removeClass('showSub');
                expand.find('.subMenu').css('top', 'auto');
            });
    }

    HoverList();
    function openSubMenu() {
        if (expand.hasClass('open')) {
            expand.on('click', function () {
                $(this).removeClass('open');
            });
        } else {
            expand.on('click', function () {
                $(this).addClass('open');
            })
        }

    }

    function ScroolBar() {
        $('.sidBarMenu .scrollBox').scrollbar();
        if($(window).outerWidth() > 768) {
            $('.tableWrapContent .scrollBox').scrollbar();
        } else {
            $('.tableWrapContent .scrollBox').scrollbar('destroy');
        }
    }
    if ($(document).find('.scrollBox')) {
        ScroolBar();
    }

    var topTablePos = $('.topPanel').outerHeight() + 120,
        bottomTablePos = $('.bottomPanel').outerHeight() + 50,
        contentHeight = $('.contentWrapper').outerHeight(),
        height = contentHeight - topTablePos - bottomTablePos;

    $('.tableWrapContent .scrollBox').css('max-height', height);

    var calendarWrap = $('.calendarBox .calendarWrapper');
    var body = $('body');
    function calendarFun() {

        if ($(window).outerWidth() < 768) {
            ClickCalendar();
            body.css('overflow', 'hidden')
        } else {
            ClickCalendar();
            body.css('overflow', 'auto')
        }

        $(function() {
            $('#date_range').datepicker({
                range: 'period',
                autoSize: true,
                numberOfMonths: 2,
                onSelect: function(dateText, inst, extensionRange) {
                    $('[data-date=startDate]').text(extensionRange.startDateText);
                    $('[data-date=endDate]').text(extensionRange.endDateText);
                }
            });

            $('#date_range').datepicker('setDate', ['+4d', '+8d']);
            $('#date_range').datepicker("option", "dateFormat", "MM dd yy");

            var extensionRange = $('#date_range').datepicker('widget').data('datepickerExtensionRange');
            if(extensionRange.startDateText) $('[data-date=startDate]').val(extensionRange.startDateText);
            if(extensionRange.endDateText) $('[data-date=endDate]').val(extensionRange.endDateText);
        });

        $('#CancelBtn, #okBtn').on('click', function () {
            calendarWrap.hide();
            body.css('overflow', 'auto')
        })
    }
    calendarFun();

    function ClickCalendar() {
        $('#calendarPopup').on('click', function () {
            calendarWrap.slideToggle();
            $(this).toggleClass('active')
        });
    }

    function Drop() {
        if ($(window).outerWidth() < 768) {
            $('#showDrop').on('click', function () {
                $(this).closest('.rightBlock').find('.rightWrap').slideToggle();
            })
        }
    }
    Drop();
    $(window).resize(function () {
        ScroolBar();
        Drop();
    });
});
